# TP algorithmie

## Consignes

Pour des raisons pratiques  (maintenance, évolutions), votre entreprise vous demande de reprendre un projet déjà fonctionnel executable sur navigateur et de le transformer  en projet node, toujours executable sur navigateur, sans changement pour l'instant.

En identifiant les principaux éléments du projet (classes, variables, fonctions) dans le fichier script.js, vous  établirez une structure de dossiers pertinente afin d'accueillir les  nouveaux fichiers que vous créerez. Vous copierez les éléments du code  existant dans ces fichiers, et avec les import et export disponible  grâce à node, le projet restera fonctionel.

Vous utiliserez un bundler (Parcel ou  Webpack) pour vérifier lors du processus que tout se passe bien, et qu'à la fin, vous êtes capable de builder afin de produires les assets  statiques qui permettent de lancer le projet dans un navigateur.

**Demande de dernière minute de votre supérieur** : le jeu doit commencer par un menu où il est possible de choisir le niveau de  difficulté (easy, medium, hard, impossible).

### Livrables

**Fichiers disponibles dans** `_rsc-exercice/`

- Des slides permettant à votre hiérarchie de comprendre la structure du projet

- La justification de votre choix concernant le bundler utilisé (en anglais pour les partenaires étrangers) 



## Réalisation

### Initialiser le projet

```shell
git clone git@gitlab.com:kevin.wolff/exo-algorithmie-stage-3.git
npm install
npx webpack --watch
```

Projet initialisé avec [CreateMyProject.sh](https://gitlab.com/kevin.wolff/create-my-project.sh) (projet personnel).