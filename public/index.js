/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/sass/index.scss":
/*!*****************************!*\
  !*** ./src/sass/index.scss ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/sass/index.scss?");

/***/ }),

/***/ "./src/js/character/Character.js":
/*!***************************************!*\
  !*** ./src/js/character/Character.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Character)\n/* harmony export */ });\nclass Character {\n    constructor(originalX, originalY, constant) {\n        this.posX = originalX\n        this.posY = originalY\n        this.constant = constant\n        this.wallsInfo = constant.wallsInfos\n        this.direction = null\n    }\n\n    // Getters.\n    getPosX = () => this.posX\n    getPosY = () => this.posY\n\n    // Vertical floor limit detector.\n    detectLimitVerticalFloor = nextVerticalMove => !(nextVerticalMove <= 0 || (nextVerticalMove + 50) >= this.constant.heightFloor)\n\n    // Horizontal floor limit detector.\n    detectLimitHorizontalFloor = nextHorizontalMove => !((nextHorizontalMove - 25) <= 0 || (nextHorizontalMove + 25) >= 700)\n\n    // Vertical wall limit detector.\n    detectVerticalWalls = (nextVerticalMove, nextHorizontalMove) => {\n        let result = true\n        this.wallsInfo.forEach(wall => {\n            if (\n                (nextVerticalMove <= (this.constant.heightFloor - wall.top + this.constant.wallDistance) && nextVerticalMove >= (this.constant.heightFloor - wall.top)) ||\n                ((nextVerticalMove + 50) >= (this.constant.heightFloor - wall.top - wall.height - this.constant.wallDistance) && ((nextVerticalMove + 50) <= ((this.constant.heightFloor - wall.top - wall.height))))\n            ) {\n                if (\n                    ((nextHorizontalMove + 25) >= wall.left && (nextHorizontalMove - 25) <= (wall.left + wall.width))\n                ) {\n                    result = false\n                }\n            }\n        })\n        return result\n    }\n\n    // Horizontal wall limit detector.\n    detectHorizontalWalls = (nextHorizontalMove, nextVerticalMove) => {\n        let result = true\n        this.wallsInfo.forEach(wall => {\n            if (\n                (((nextHorizontalMove + 25) >= (wall.left - this.constant.wallDistance)) && ((nextHorizontalMove + 25) <= wall.left)) ||\n                (((nextHorizontalMove - 25) >= (wall.left + wall.width)) && ((nextHorizontalMove - 25) <= (wall.left + wall.width + this.constant.wallDistance)))\n            ) {\n                if (\n                    ((nextVerticalMove + 25) <= (this.constant.heightFloor - wall.top)) && (nextVerticalMove >= (this.constant.heightFloor - wall.top - wall.height))\n                ) {\n                    result = false\n                }\n            }\n        })\n        return result\n    }\n\n    // Handle player action.\n    handleMove = (move, step) => {\n        switch (move) {\n            case 'ArrowUp':\n                if (this.detectLimitVerticalFloor(this.posY + step) && this.detectVerticalWalls(this.posY + step, this.posX)) {\n                    this.posY = this.posY + step\n                    this.direction = 'ArrowUp'\n                }\n                break\n            case 'ArrowDown':\n                if (this.detectLimitVerticalFloor(this.posY - step) && this.detectVerticalWalls(this.posY - step, this.posX)) {\n                    this.posY = this.posY - step\n                    this.direction = 'ArrowDown'\n                }\n                break\n            case 'ArrowRight':\n                if (this.detectLimitHorizontalFloor(this.posX + step) && this.detectHorizontalWalls(this.posX + step, this.posY)) {\n                    this.posX = this.posX + step\n                    this.direction = 'ArrowRight'\n                }\n                break\n            case 'ArrowLeft':\n                if (this.detectLimitHorizontalFloor(this.posX - step) && this.detectHorizontalWalls(this.posX - step, this.posY)) {\n                    this.posX = this.posX - step\n                    this.direction = 'ArrowLeft'\n                }\n                break\n            default:\n                break\n        }\n    }\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/character/Character.js?");

/***/ }),

/***/ "./src/js/character/Ghost.js":
/*!***********************************!*\
  !*** ./src/js/character/Ghost.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Ghost)\n/* harmony export */ });\n/* harmony import */ var _Character_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character.js */ \"./src/js/character/Character.js\");\n/* harmony import */ var _dialogue_dialogueGameOver_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../dialogue/dialogueGameOver.js */ \"./src/js/dialogue/dialogueGameOver.js\");\n\n\n\nclass Ghost extends _Character_js__WEBPACK_IMPORTED_MODULE_0__.default {\n    constructor(originalX, originalY, constant, pacman, floor) {\n        const _super = super(originalX, originalY, constant)\n        this._super = _super\n\n        this.constant = constant\n        this.pac = pacman\n        this.floor = floor\n        this.countTop = 0\n        this.countRight = 0\n        this.switchLeft = false\n        this.switchTop = false\n        this.intervalId = null\n        this.biasDirectionX = Math.floor(Math.random() * 100) % 2 == 0 ? 'ArrowRight' : 'ArrowLeft'\n        this.biasDirectionY = Math.floor(Math.random() * 100) % 2 == 0 ? 'ArrowUp' : 'ArrowDown'\n\n        const i = Math.floor(Math.random() * 4)\n\n        // Ghost body.\n        this.ghost = document.createElement('div')\n        this.ghost.setAttribute('class', 'ghost')\n        this.ghost.style.backgroundColor = constant.colorsGhost[i]\n        this.ghost.style.left = `${this.posX - 25}px`\n        this.ghost.style.bottom = `${this.posY}px`\n\n        // Ghost eyes.\n        const eyes = document.createElement('div')\n        eyes.setAttribute('class', 'eyes')\n\n        // Ghost eye.\n        const eye = document.createElement('div')\n        eye.setAttribute('class', 'eye')\n\n        // Ghost iris.\n        const iris = document.createElement('div')\n        iris.setAttribute('class', 'iris')\n\n        // Ghost tail.\n        const tail = document.createElement('div')\n        tail.setAttribute('class', 'tail')\n        tail.style.background = `linear-gradient(-45deg, transparent 75%, ${constant.colorsGhost[i]} 75%) 0 50%, linear-gradient( 45deg, transparent 75%, ${constant.colorsGhost[i]} 75%) 0 50%`\n\n        // Clone eye and iris.\n        const eye2 = eye.cloneNode()\n        const iris2 = iris.cloneNode()\n\n        // Append DOM element.\n        this.ghost.append(eyes, tail)\n        eyes.append(eye, eye2)\n        eye.appendChild(iris)\n        eye2.appendChild(iris2)\n\n        this.initialization(this)\n    }\n\n    // Initialization.\n    initialization = (_self) => {\n        _self.intervalId = setInterval(() => {\n            function getRandomMove() {\n                let randomIndex = Math.floor(Math.random() * 4)\n\n                // Incorporating bias.\n                const rand1 = Math.floor(Math.random() * 100)\n                const rand2 = Math.floor(Math.random() * 100)\n                randomIndex = (randomIndex === 2 && rand1 % 2 === 0) ? 3 : randomIndex\n                randomIndex = (randomIndex === 1 && rand2 % 2 === 0) ? 0 : randomIndex\n                let randomMove = _self.constant.directions[randomIndex]\n\n                if (randomMove === 'ArrowUp' || randomMove === 'ArrowDown') {\n\n                    if (_self.countTop < _self.constant.ghostLimitStraightLine && !_self.switchTop) {\n                        randomMove = _self.biasDirectionY\n                        _self.countTop += 1\n                    } else if (_self.countTop === _self.constant.ghostLimitStraightLine && !_self.switchTop) {\n                        _self.countTop = 0\n                        _self.switchTop = true\n                    }\n\n                    if (_self.countTop < _self.constant.ghostLimitStraightLine && _self.switchTop) {\n                        randomMove = _self.biasDirectionY === 'ArrowDown' ? 'ArrowUp' : 'ArrowDown'\n                        _self.countTop += 1\n                    } else if (_self.countTop === _self.constant.ghostLimitStraightLine && _self.switchTop) {\n                        _self.countTop = 0\n                        _self.switchTop = false\n                    }\n                }\n\n                if (randomMove === 'ArrowLeft' || randomMove === 'ArrowRight') {\n\n                    if (_self.countRight < _self.constant.ghostLimitStraightLine && !_self.switchLeft) {\n                        randomMove = _self.biasDirectionX\n                        _self.countRight += 1\n                    } else if (_self.countRight === _self.constant.ghostLimitStraightLine && !_self.switchLeft) {\n                        _self.countRight = 0\n                        _self.switchLeft = true\n                    }\n\n                    if (_self.countRight < _self.constant.ghostLimitStraightLine && _self.switchLeft) {\n                        randomMove = _self.biasDirectionX === 'ArrowRight' ? 'ArrowLeft' : 'ArrowRight'\n                        _self.countRight += 1\n                    } else if (_self.countRight === _self.constant.ghostLimitStraightLine && _self.switchLeft) {\n                        _self.countRight = 0\n                        _self.switchLeft = false\n                    }\n                }\n                _self._super.handleMove(randomMove, _self.constant.stepGhost)\n                _self.updatePosition()\n            }\n\n            getRandomMove()\n        }, 70)\n    }\n\n    // Getter.\n    getGhost = () => this.ghost\n\n    // Update position.\n    updatePosition = () => {\n\n        // Check if the ghost hit pacman vertically.\n        if (Math.abs(this.posY - this.pac.getPosY() - this.constant.sizePacman) < 15 ||\n            Math.abs(this.pac.getPosY() - this.posY - this.constant.sizePacman) < 15\n        ) {\n            // Check if pacman is align with ghost laterally.\n            if (Math.abs(this.posX - this.pac.getPosX()) < 25) {\n                clearInterval(this.intervalId)\n                ;(0,_dialogue_dialogueGameOver_js__WEBPACK_IMPORTED_MODULE_1__.default)(this.floor)\n            }\n        }\n\n        // Check if the ghost hit pacman laterally.\n        if (Math.abs(this.pac.getPosX() + this.constant.sizePacman - this.posX) < 10 ||\n            Math.abs(this.posX + this.constant.widthGhost - this.pac.getPosX()) < 10\n        ) {\n            // Check if pacman is align with ghost vertically.\n            if (Math.abs(this.pac.getPosY() - this.posY) < 25) {\n                clearInterval(this.intervalId)\n                ;(0,_dialogue_dialogueGameOver_js__WEBPACK_IMPORTED_MODULE_1__.default)(this.floor)\n            }\n        }\n        this.ghost.style.left = `${this.posX - 25}px`\n        this.ghost.style.bottom = `${this.posY}px`\n    }\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/character/Ghost.js?");

/***/ }),

/***/ "./src/js/character/Pacman.js":
/*!************************************!*\
  !*** ./src/js/character/Pacman.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Pacman)\n/* harmony export */ });\n/* harmony import */ var _Character_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character.js */ \"./src/js/character/Character.js\");\n/* harmony import */ var _dialogue_dialogueWin_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../dialogue/dialogueWin.js */ \"./src/js/dialogue/dialogueWin.js\");\n\n\n\nclass Pacman extends _Character_js__WEBPACK_IMPORTED_MODULE_0__.default {\n    constructor(originalX, originalY, constant, floor) {\n        const _super = super(originalX, originalY, constant)\n        this._super = _super\n\n        this.constant = constant\n        this.foodsInfo = constant.foodsPosition\n        this.floor = floor\n        this.countFoods = 0\n\n        // Pacman body.\n        this.pacman = document.createElement('div')\n        this.pacman.setAttribute('id', 'pacman')\n        this.pacman.style.left = `${this.posX - 25}px`\n        this.pacman.style.bottom = `${this.posY}px`\n\n        // Pacman mouth.\n        const mouth = document.createElement('div')\n        mouth.setAttribute('id', 'packman-mouth')\n\n        // Append DOM element.\n        this.pacman.appendChild(mouth)\n\n        this.initialization(this)\n    }\n\n    // Initialization.\n    initialization = (_self) => {\n        document.addEventListener('keydown', function (event) {\n            if (_self.constant.directions.includes(event.key)) {\n                _self._super.handleMove(event.key, _self.constant.step)\n                _self.updatePosition()\n            }\n        })\n    }\n\n    // Getter.\n    getPacMan = () => this.pacman\n\n    // Handle eat food action.\n    eatFood = () => {\n        this.foodsInfo.forEach((f, i) => {\n\n            // Check if pacman eat food vertically\n            if (Math.abs((this.constant.heightFloor - f.top + this.constant.foodHeight) - this.posY) <= 3 ||\n                Math.abs(this.posY + this.constant.sizePacman - (this.constant.heightFloor - f.top)) <= 3) {\n\n                // Check if pacman is align with food laterally\n                if (Math.abs(this.posX - f.left) < 10) {\n                    const foodEl = document.getElementById(f.id);\n                    if (foodEl) {\n                        foodEl.style.display = 'none'\n                        if (!this.constant.foodAte.includes(f.id)) {\n                            this.countFoods += 1\n                            this.constant.foodAte.push(f.id)\n                        }\n                        if (this.countFoods === this.constant.maxFood) {\n                            (0,_dialogue_dialogueWin_js__WEBPACK_IMPORTED_MODULE_1__.default)(this.floor)\n                        }\n                    }\n                }\n            }\n\n            // Check if pacman eat food laterally\n            if (Math.abs(this.posX + this.constant.sizePacman - f.left) <= 3 ||\n                Math.abs(f.left + this.constant.foodWidth - this.posX) <= 3) {\n\n                // Check if pacman is align with food vertically\n                if (Math.abs(this.posY - (this.constant.heightFloor - f.top)) <= 40) {\n                    const foodEl = document.getElementById(f.id)\n                    if (foodEl) {\n                        foodEl.style.display = 'none'\n                        if (!this.constant.foodAte.includes(f.id)) {\n                            this.countFoods += 1\n                            this.constant.foodAte.push(f.id)\n                        }\n                        if (this.countFoods === this.constant.maxFood) {\n                            (0,_dialogue_dialogueWin_js__WEBPACK_IMPORTED_MODULE_1__.default)(this.floor)\n                        }\n                    }\n                }\n            }\n\n        })\n    }\n\n    // Update position.\n    updatePosition = () => {\n        this.pacman.style.transform = `rotate(${this.constant.angleDirection[this.direction]}deg)`\n        this.pacman.style.left = `${this.posX - 25}px`\n        this.pacman.style.bottom = `${this.posY}px`\n        this.eatFood()\n    }\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/character/Pacman.js?");

/***/ }),

/***/ "./src/js/dialogue/dialogueGameOver.js":
/*!*********************************************!*\
  !*** ./src/js/dialogue/dialogueGameOver.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ dialogueGameOver)\n/* harmony export */ });\n// Game over dialogue.\nfunction dialogueGameOver(floor) {\n\n    const card = document.createElement('div')\n    card.setAttribute('class', 'modal')\n\n    const text = document.createElement('div')\n    text.setAttribute('class', 'txt')\n    text.innerHTML = 'Game Over'\n\n    card.appendChild(text)\n    floor.appendChild(card)\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/dialogue/dialogueGameOver.js?");

/***/ }),

/***/ "./src/js/dialogue/dialogueLauncher.js":
/*!*********************************************!*\
  !*** ./src/js/dialogue/dialogueLauncher.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ dialogueLauncher)\n/* harmony export */ });\n// Launcher dialogue.\nfunction dialogueLauncher() {\n\n    const difficulties = ['easy', 'medium', 'hard', 'impossible']\n\n    const launcher = document.createElement('div')\n    launcher.setAttribute('class', 'modal')\n\n    difficulties.forEach((difficulty, i) => {\n        const button = document.createElement('div')\n        button.setAttribute('class', 'btn')\n        button.innerHTML = `${difficulty}`\n\n        launcher.appendChild(button)\n    })\n\n    return launcher\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/dialogue/dialogueLauncher.js?");

/***/ }),

/***/ "./src/js/dialogue/dialogueWin.js":
/*!****************************************!*\
  !*** ./src/js/dialogue/dialogueWin.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ dialogueWin)\n/* harmony export */ });\n// Win dialogue.\nfunction dialogueWin(floor) {\n\n    const card = document.createElement('div')\n    card.setAttribute('class', 'modal')\n\n    const text = document.createElement('div')\n    text.setAttribute('class', 'txt')\n    text.innerHTML = \"You won!!\"\n\n    card.appendChild(text)\n    floor.appendChild(card)\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/dialogue/dialogueWin.js?");

/***/ }),

/***/ "./src/js/environment/generateFloor.js":
/*!*********************************************!*\
  !*** ./src/js/environment/generateFloor.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ generateFloor)\n/* harmony export */ });\n// Generate floor.\nfunction generateFloor() {\n\n    const floor = document.createElement('div')\n    floor.setAttribute('id', 'floor')\n\n    return floor\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/environment/generateFloor.js?");

/***/ }),

/***/ "./src/js/environment/generateFood.js":
/*!********************************************!*\
  !*** ./src/js/environment/generateFood.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ generateFood)\n/* harmony export */ });\n// Generate food.\nfunction generateFood(foodsPosition, foodWidth, foodHeight, foodColor) {\n\n    const foods = []\n\n    foodsPosition.map((p, i) => {\n        const f = document.createElement('div')\n        f.setAttribute('id', p.id)\n        f.style.width = `${foodWidth}px`\n        f.style.height = `${foodHeight}px`\n        f.style.backgroundColor = foodColor\n        f.style.position = \"absolute\"\n        f.style.top = `${p.top}px`\n        f.style.left = `${p.left}px`\n\n        foods.push(f)\n    })\n\n    return foods\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/environment/generateFood.js?");

/***/ }),

/***/ "./src/js/environment/generateGhost.js":
/*!*********************************************!*\
  !*** ./src/js/environment/generateGhost.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ generateGhosts)\n/* harmony export */ });\n/* harmony import */ var _character_Ghost_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../character/Ghost.js */ \"./src/js/character/Ghost.js\");\n\n\n// Generate ghost.\nfunction generateGhosts(constant, floor, pacman, difficulty) {\n\n    let interval\n\n    switch (difficulty) {\n        case 'easy':\n            interval = 10000\n            break\n        case 'medium':\n            interval = 7000\n            break\n        case 'hard':\n            interval = 3500\n            break\n        case 'impossible':\n            interval = 1000\n            break\n    }\n\n    const ghost1 = new _character_Ghost_js__WEBPACK_IMPORTED_MODULE_0__.default(350, 350, constant, pacman, floor)\n    floor.appendChild(ghost1.getGhost())\n\n    setInterval(() => {\n        floor.appendChild(new _character_Ghost_js__WEBPACK_IMPORTED_MODULE_0__.default(350, 350, constant, pacman, floor).getGhost())\n    }, interval)\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/environment/generateGhost.js?");

/***/ }),

/***/ "./src/js/environment/generateScreen.js":
/*!**********************************************!*\
  !*** ./src/js/environment/generateScreen.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ generateScreen)\n/* harmony export */ });\n// Generate screen.\nfunction generateScreen() {\n\n    const screen = document.createElement('div')\n    screen.setAttribute('id', 'screen')\n\n    return screen\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/environment/generateScreen.js?");

/***/ }),

/***/ "./src/js/environment/generateWall.js":
/*!********************************************!*\
  !*** ./src/js/environment/generateWall.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ generateWall)\n/* harmony export */ });\n// Generate wall.\nfunction generateWall(wallsArray) {\n\n    const walls = []\n\n    wallsArray.map((wall, i) => {\n        const w = document.createElement('div')\n        w.setAttribute('id', `wall-${i}`)\n        w.style.width = `${wall.width}px`\n        w.style.height = `${wall.height}px`\n        w.style.border = '#3F51B5 7px double'\n        w.style.boxSizing = 'border-box'\n        w.style.borderRadius = '2px'\n        w.style.backgroundColor = 'black'\n        w.style.position = 'absolute'\n        w.style.top = `${wall.top}px`\n        w.style.left = `${wall.left}px`\n\n        walls.push(w)\n    })\n\n    return walls\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/environment/generateWall.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _util_constant_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util/constant.js */ \"./src/js/util/constant.js\");\n/* harmony import */ var _character_Pacman_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./character/Pacman.js */ \"./src/js/character/Pacman.js\");\n/* harmony import */ var _dialogue_dialogueLauncher_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dialogue/dialogueLauncher.js */ \"./src/js/dialogue/dialogueLauncher.js\");\n/* harmony import */ var _environment_generateScreen_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environment/generateScreen.js */ \"./src/js/environment/generateScreen.js\");\n/* harmony import */ var _environment_generateFloor_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environment/generateFloor.js */ \"./src/js/environment/generateFloor.js\");\n/* harmony import */ var _environment_generateWall_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./environment/generateWall.js */ \"./src/js/environment/generateWall.js\");\n/* harmony import */ var _environment_generateFood_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./environment/generateFood.js */ \"./src/js/environment/generateFood.js\");\n/* harmony import */ var _environment_generateGhost_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./environment/generateGhost.js */ \"./src/js/environment/generateGhost.js\");\n/* harmony import */ var _sass_index_scss__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../sass/index.scss */ \"./src/sass/index.scss\");\n\n\n\n\n\n\n\n\n\n\nconst root = document.querySelector('#root')\n\nlet launcher = (0,_dialogue_dialogueLauncher_js__WEBPACK_IMPORTED_MODULE_2__.default)()\nroot.append(launcher)\n\nlauncher.childNodes.forEach(difficultyButton => {\n\n    // Listen each difficulty button.\n    difficultyButton.addEventListener('click', () => {\n\n        // When player choose a difficulty.\n        const gameDifficulty = difficultyButton.innerHTML\n        launcher.remove()\n        initializeGame(gameDifficulty)\n    })\n})\n\n// Initialize a game.\nfunction initializeGame(difficulty) {\n    const screen = (0,_environment_generateScreen_js__WEBPACK_IMPORTED_MODULE_3__.default)()\n    const floor = (0,_environment_generateFloor_js__WEBPACK_IMPORTED_MODULE_4__.default)()\n    const walls = (0,_environment_generateWall_js__WEBPACK_IMPORTED_MODULE_5__.default)(_util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant.wallsInfos)\n    const foods = (0,_environment_generateFood_js__WEBPACK_IMPORTED_MODULE_6__.default)(_util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant.foodsPosition, _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant.foodWidth, _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant.foodHeight, _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant.foodColor)\n    const pacman = new _character_Pacman_js__WEBPACK_IMPORTED_MODULE_1__.default(350, 50, _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant, floor)\n\n    root.append(screen)\n    screen.appendChild(floor)\n    walls.forEach(wall => floor.appendChild(wall))\n    foods.forEach(food => floor.appendChild(food))\n    floor.appendChild(pacman.getPacMan())\n    ;(0,_environment_generateGhost_js__WEBPACK_IMPORTED_MODULE_7__.default)(_util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constant, floor, pacman, difficulty)\n}\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/index.js?");

/***/ }),

/***/ "./src/js/util/constant.js":
/*!*********************************!*\
  !*** ./src/js/util/constant.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports) => {

eval("// Global variable.\nconst constant = {\n\n    // Environment.\n    wallDistance: 2,\n    widthFloor: 700,\n    heightFloor: 700,\n\n    // Pacman.\n    sizePacman: 50,\n    step: 5,\n    foodAte: [],\n    maxFood: 88,\n\n    // Ghost.\n    widthGhost: 35,\n    heightGhost: 40,\n    colorsGhost: ['cyan', '#f5b041', '#e74c3c', '#e8daef'],\n    ghostLimitStraightLine: 100,\n    stepGhost: 5,\n\n    // Food.\n    foodWidth: 10,\n    foodHeight: 10,\n    foodColor: '#f3f1d6',\n\n    // Directions.\n    directions: ['ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'],\n    angleDirection: {\n        ArrowUp: -90,\n        ArrowDown: 90,\n        ArrowLeft: 180,\n        ArrowRight: 0,\n    },\n\n    // Walls.\n    wallsInfos: [\n        {width: 650, height: 30, left: 15, top: 20},\n        {width: 650, height: 30, left: 15, top: 650},\n        {width: 30, height: 300, left: 15, top: 20},\n        {width: 30, height: 250, left: 15, top: 430},\n        {width: 30, height: 300, left: 650, top: 20},\n        {width: 30, height: 250, left: 650, top: 430},\n        {width: 70, height: 180, left: 130, top: 260},\n        {width: 70, height: 180, left: 480, top: 260},\n        {width: 100, height: 70, left: 120, top: 520},\n        {width: 100, height: 70, left: 460, top: 520},\n        {width: 100, height: 20, left: 290, top: 570},\n        {width: 100, height: 70, left: 120, top: 110},\n        {width: 100, height: 70, left: 460, top: 110},\n        {width: 100, height: 20, left: 290, top: 110},\n        {width: 10, height: 178, left: 270, top: 260},\n        {width: 10, height: 178, left: 400, top: 260},\n    ],\n\n    // Foods.\n    foodsPosition: [\n        {top: 70, left: 80, id: 1},\n        {top: 110, left: 80, id: 2},\n        {top: 150, left: 80, id: 3},\n        {top: 190, left: 80, id: 4},\n        {top: 230, left: 80, id: 5},\n        {top: 270, left: 80, id: 6},\n        {top: 310, left: 80, id: 7},\n        {top: 350, left: 80, id: 8},\n        {top: 390, left: 80, id: 9},\n        {top: 430, left: 80, id: 10},\n        {top: 470, left: 80, id: 11},\n        {top: 510, left: 80, id: 12},\n        {top: 550, left: 80, id: 13},\n        {top: 590, left: 80, id: 14},\n        {top: 615, left: 80, id: 15},\n\n        {top: 615, left: 120, id: 16},\n        {top: 615, left: 160, id: 17},\n        {top: 615, left: 200, id: 18},\n        {top: 615, left: 240, id: 19},\n        {top: 615, left: 280, id: 20},\n        {top: 615, left: 320, id: 21},\n        {top: 615, left: 360, id: 22},\n        {top: 615, left: 400, id: 23},\n        {top: 615, left: 440, id: 24},\n        {top: 615, left: 480, id: 25},\n        {top: 615, left: 520, id: 26},\n        {top: 615, left: 560, id: 27},\n\n        {top: 615, left: 610, id: 28},\n        {top: 590, left: 610, id: 29},\n        {top: 550, left: 610, id: 30},\n        {top: 510, left: 610, id: 31},\n        {top: 470, left: 610, id: 32},\n        {top: 430, left: 610, id: 33},\n        {top: 390, left: 610, id: 34},\n        {top: 350, left: 610, id: 35},\n        {top: 310, left: 610, id: 36},\n        {top: 270, left: 610, id: 37},\n        {top: 230, left: 610, id: 38},\n        {top: 190, left: 610, id: 39},\n        {top: 150, left: 610, id: 40},\n        {top: 110, left: 610, id: 41},\n        {top: 70, left: 610, id: 42},\n\n        {top: 70, left: 120, id: 43},\n        {top: 70, left: 160, id: 44},\n        {top: 70, left: 200, id: 45},\n        {top: 70, left: 240, id: 46},\n        {top: 70, left: 280, id: 47},\n        {top: 70, left: 320, id: 48},\n        {top: 70, left: 360, id: 49},\n        {top: 70, left: 400, id: 50},\n        {top: 70, left: 440, id: 51},\n        {top: 70, left: 480, id: 52},\n        {top: 70, left: 520, id: 53},\n        {top: 70, left: 560, id: 54},\n\n        {top: 215, left: 120, id: 55},\n        {top: 215, left: 160, id: 56},\n        {top: 215, left: 200, id: 57},\n        {top: 215, left: 240, id: 58},\n        {top: 215, left: 280, id: 59},\n        {top: 215, left: 320, id: 60},\n        {top: 215, left: 360, id: 61},\n        {top: 215, left: 400, id: 62},\n        {top: 215, left: 440, id: 63},\n        {top: 215, left: 480, id: 64},\n        {top: 215, left: 520, id: 65},\n        {top: 215, left: 560, id: 66},\n\n        {top: 475, left: 120, id: 67},\n        {top: 475, left: 160, id: 68},\n        {top: 475, left: 200, id: 69},\n        {top: 475, left: 240, id: 70},\n        {top: 475, left: 280, id: 71},\n        {top: 475, left: 320, id: 72},\n        {top: 475, left: 360, id: 73},\n        {top: 475, left: 400, id: 74},\n        {top: 475, left: 440, id: 75},\n        {top: 475, left: 480, id: 76},\n        {top: 475, left: 520, id: 77},\n        {top: 475, left: 560, id: 78},\n\n        {top: 430, left: 440, id: 79},\n        {top: 390, left: 440, id: 80},\n        {top: 350, left: 440, id: 81},\n        {top: 310, left: 440, id: 82},\n        {top: 270, left: 440, id: 83},\n\n        {top: 430, left: 230, id: 84},\n        {top: 390, left: 230, id: 85},\n        {top: 350, left: 230, id: 86},\n        {top: 310, left: 230, id: 87},\n        {top: 270, left: 230, id: 88},\n    ]\n}\n\nexports.constant = constant\n\n//# sourceURL=webpack://exo-algorithmie-stage-3/./src/js/util/constant.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/index.js");
/******/ 	
/******/ })()
;