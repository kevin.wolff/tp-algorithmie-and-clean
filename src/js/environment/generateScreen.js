// Generate screen.
export default function generateScreen() {

    const screen = document.createElement('div')
    screen.setAttribute('id', 'screen')

    return screen
}