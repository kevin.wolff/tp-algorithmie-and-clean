// Generate wall.
export default function generateWall(wallsArray) {

    const walls = []

    wallsArray.map((wall, i) => {
        const w = document.createElement('div')
        w.setAttribute('id', `wall-${i}`)
        w.style.width = `${wall.width}px`
        w.style.height = `${wall.height}px`
        w.style.border = '#3F51B5 7px double'
        w.style.boxSizing = 'border-box'
        w.style.borderRadius = '2px'
        w.style.backgroundColor = 'black'
        w.style.position = 'absolute'
        w.style.top = `${wall.top}px`
        w.style.left = `${wall.left}px`

        walls.push(w)
    })

    return walls
}