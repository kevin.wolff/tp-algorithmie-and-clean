import Ghost from '../character/Ghost.js'

// Generate ghost.
export default function generateGhosts(constant, floor, pacman, difficulty) {

    let interval

    switch (difficulty) {
        case 'easy':
            interval = 10000
            break
        case 'medium':
            interval = 7000
            break
        case 'hard':
            interval = 3500
            break
        case 'impossible':
            interval = 1000
            break
    }

    const ghost1 = new Ghost(350, 350, constant, pacman, floor)
    floor.appendChild(ghost1.getGhost())

    setInterval(() => {
        floor.appendChild(new Ghost(350, 350, constant, pacman, floor).getGhost())
    }, interval)
}