// Generate floor.
export default function generateFloor() {

    const floor = document.createElement('div')
    floor.setAttribute('id', 'floor')

    return floor
}