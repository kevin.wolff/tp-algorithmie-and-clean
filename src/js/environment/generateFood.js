// Generate food.
export default function generateFood(foodsPosition, foodWidth, foodHeight, foodColor) {

    const foods = []

    foodsPosition.map((p, i) => {
        const f = document.createElement('div')
        f.setAttribute('id', p.id)
        f.style.width = `${foodWidth}px`
        f.style.height = `${foodHeight}px`
        f.style.backgroundColor = foodColor
        f.style.position = "absolute"
        f.style.top = `${p.top}px`
        f.style.left = `${p.left}px`

        foods.push(f)
    })

    return foods
}