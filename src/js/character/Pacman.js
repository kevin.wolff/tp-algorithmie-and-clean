import Character from './Character.js'
import dialogueWin from '../dialogue/dialogueWin.js'

export default class Pacman extends Character {
    constructor(originalX, originalY, constant, floor) {
        const _super = super(originalX, originalY, constant)
        this._super = _super

        this.constant = constant
        this.foodsInfo = constant.foodsPosition
        this.floor = floor
        this.countFoods = 0

        // Pacman body.
        this.pacman = document.createElement('div')
        this.pacman.setAttribute('id', 'pacman')
        this.pacman.style.left = `${this.posX - 25}px`
        this.pacman.style.bottom = `${this.posY}px`

        // Pacman mouth.
        const mouth = document.createElement('div')
        mouth.setAttribute('id', 'packman-mouth')

        // Append DOM element.
        this.pacman.appendChild(mouth)

        this.initialization(this)
    }

    // Initialization.
    initialization = (_self) => {
        document.addEventListener('keydown', function (event) {
            if (_self.constant.directions.includes(event.key)) {
                _self._super.handleMove(event.key, _self.constant.step)
                _self.updatePosition()
            }
        })
    }

    // Getter.
    getPacMan = () => this.pacman

    // Handle eat food action.
    eatFood = () => {
        this.foodsInfo.forEach((f, i) => {

            // Check if pacman eat food vertically
            if (Math.abs((this.constant.heightFloor - f.top + this.constant.foodHeight) - this.posY) <= 3 ||
                Math.abs(this.posY + this.constant.sizePacman - (this.constant.heightFloor - f.top)) <= 3) {

                // Check if pacman is align with food laterally
                if (Math.abs(this.posX - f.left) < 10) {
                    const foodEl = document.getElementById(f.id);
                    if (foodEl) {
                        foodEl.style.display = 'none'
                        if (!this.constant.foodAte.includes(f.id)) {
                            this.countFoods += 1
                            this.constant.foodAte.push(f.id)
                        }
                        if (this.countFoods === this.constant.maxFood) {
                            dialogueWin(this.floor)
                        }
                    }
                }
            }

            // Check if pacman eat food laterally
            if (Math.abs(this.posX + this.constant.sizePacman - f.left) <= 3 ||
                Math.abs(f.left + this.constant.foodWidth - this.posX) <= 3) {

                // Check if pacman is align with food vertically
                if (Math.abs(this.posY - (this.constant.heightFloor - f.top)) <= 40) {
                    const foodEl = document.getElementById(f.id)
                    if (foodEl) {
                        foodEl.style.display = 'none'
                        if (!this.constant.foodAte.includes(f.id)) {
                            this.countFoods += 1
                            this.constant.foodAte.push(f.id)
                        }
                        if (this.countFoods === this.constant.maxFood) {
                            dialogueWin(this.floor)
                        }
                    }
                }
            }

        })
    }

    // Update position.
    updatePosition = () => {
        this.pacman.style.transform = `rotate(${this.constant.angleDirection[this.direction]}deg)`
        this.pacman.style.left = `${this.posX - 25}px`
        this.pacman.style.bottom = `${this.posY}px`
        this.eatFood()
    }
}