export default class Character {
    constructor(originalX, originalY, constant) {
        this.posX = originalX
        this.posY = originalY
        this.constant = constant
        this.wallsInfo = constant.wallsInfos
        this.direction = null
    }

    // Getters.
    getPosX = () => this.posX
    getPosY = () => this.posY

    // Vertical floor limit detector.
    detectLimitVerticalFloor = nextVerticalMove => !(nextVerticalMove <= 0 || (nextVerticalMove + 50) >= this.constant.heightFloor)

    // Horizontal floor limit detector.
    detectLimitHorizontalFloor = nextHorizontalMove => !((nextHorizontalMove - 25) <= 0 || (nextHorizontalMove + 25) >= 700)

    // Vertical wall limit detector.
    detectVerticalWalls = (nextVerticalMove, nextHorizontalMove) => {
        let result = true
        this.wallsInfo.forEach(wall => {
            if (
                (nextVerticalMove <= (this.constant.heightFloor - wall.top + this.constant.wallDistance) && nextVerticalMove >= (this.constant.heightFloor - wall.top)) ||
                ((nextVerticalMove + 50) >= (this.constant.heightFloor - wall.top - wall.height - this.constant.wallDistance) && ((nextVerticalMove + 50) <= ((this.constant.heightFloor - wall.top - wall.height))))
            ) {
                if (
                    ((nextHorizontalMove + 25) >= wall.left && (nextHorizontalMove - 25) <= (wall.left + wall.width))
                ) {
                    result = false
                }
            }
        })
        return result
    }

    // Horizontal wall limit detector.
    detectHorizontalWalls = (nextHorizontalMove, nextVerticalMove) => {
        let result = true
        this.wallsInfo.forEach(wall => {
            if (
                (((nextHorizontalMove + 25) >= (wall.left - this.constant.wallDistance)) && ((nextHorizontalMove + 25) <= wall.left)) ||
                (((nextHorizontalMove - 25) >= (wall.left + wall.width)) && ((nextHorizontalMove - 25) <= (wall.left + wall.width + this.constant.wallDistance)))
            ) {
                if (
                    ((nextVerticalMove + 25) <= (this.constant.heightFloor - wall.top)) && (nextVerticalMove >= (this.constant.heightFloor - wall.top - wall.height))
                ) {
                    result = false
                }
            }
        })
        return result
    }

    // Handle player action.
    handleMove = (move, step) => {
        switch (move) {
            case 'ArrowUp':
                if (this.detectLimitVerticalFloor(this.posY + step) && this.detectVerticalWalls(this.posY + step, this.posX)) {
                    this.posY = this.posY + step
                    this.direction = 'ArrowUp'
                }
                break
            case 'ArrowDown':
                if (this.detectLimitVerticalFloor(this.posY - step) && this.detectVerticalWalls(this.posY - step, this.posX)) {
                    this.posY = this.posY - step
                    this.direction = 'ArrowDown'
                }
                break
            case 'ArrowRight':
                if (this.detectLimitHorizontalFloor(this.posX + step) && this.detectHorizontalWalls(this.posX + step, this.posY)) {
                    this.posX = this.posX + step
                    this.direction = 'ArrowRight'
                }
                break
            case 'ArrowLeft':
                if (this.detectLimitHorizontalFloor(this.posX - step) && this.detectHorizontalWalls(this.posX - step, this.posY)) {
                    this.posX = this.posX - step
                    this.direction = 'ArrowLeft'
                }
                break
            default:
                break
        }
    }
}