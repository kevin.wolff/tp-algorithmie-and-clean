import Character from './Character.js'
import dialogueGameOver from '../dialogue/dialogueGameOver.js'

export default class Ghost extends Character {
    constructor(originalX, originalY, constant, pacman, floor) {
        const _super = super(originalX, originalY, constant)
        this._super = _super

        this.constant = constant
        this.pac = pacman
        this.floor = floor
        this.countTop = 0
        this.countRight = 0
        this.switchLeft = false
        this.switchTop = false
        this.intervalId = null
        this.biasDirectionX = Math.floor(Math.random() * 100) % 2 == 0 ? 'ArrowRight' : 'ArrowLeft'
        this.biasDirectionY = Math.floor(Math.random() * 100) % 2 == 0 ? 'ArrowUp' : 'ArrowDown'

        const i = Math.floor(Math.random() * 4)

        // Ghost body.
        this.ghost = document.createElement('div')
        this.ghost.setAttribute('class', 'ghost')
        this.ghost.style.backgroundColor = constant.colorsGhost[i]
        this.ghost.style.left = `${this.posX - 25}px`
        this.ghost.style.bottom = `${this.posY}px`

        // Ghost eyes.
        const eyes = document.createElement('div')
        eyes.setAttribute('class', 'eyes')

        // Ghost eye.
        const eye = document.createElement('div')
        eye.setAttribute('class', 'eye')

        // Ghost iris.
        const iris = document.createElement('div')
        iris.setAttribute('class', 'iris')

        // Ghost tail.
        const tail = document.createElement('div')
        tail.setAttribute('class', 'tail')
        tail.style.background = `linear-gradient(-45deg, transparent 75%, ${constant.colorsGhost[i]} 75%) 0 50%, linear-gradient( 45deg, transparent 75%, ${constant.colorsGhost[i]} 75%) 0 50%`

        // Clone eye and iris.
        const eye2 = eye.cloneNode()
        const iris2 = iris.cloneNode()

        // Append DOM element.
        this.ghost.append(eyes, tail)
        eyes.append(eye, eye2)
        eye.appendChild(iris)
        eye2.appendChild(iris2)

        this.initialization(this)
    }

    // Initialization.
    initialization = (_self) => {
        _self.intervalId = setInterval(() => {
            function getRandomMove() {
                let randomIndex = Math.floor(Math.random() * 4)

                // Incorporating bias.
                const rand1 = Math.floor(Math.random() * 100)
                const rand2 = Math.floor(Math.random() * 100)
                randomIndex = (randomIndex === 2 && rand1 % 2 === 0) ? 3 : randomIndex
                randomIndex = (randomIndex === 1 && rand2 % 2 === 0) ? 0 : randomIndex
                let randomMove = _self.constant.directions[randomIndex]

                if (randomMove === 'ArrowUp' || randomMove === 'ArrowDown') {

                    if (_self.countTop < _self.constant.ghostLimitStraightLine && !_self.switchTop) {
                        randomMove = _self.biasDirectionY
                        _self.countTop += 1
                    } else if (_self.countTop === _self.constant.ghostLimitStraightLine && !_self.switchTop) {
                        _self.countTop = 0
                        _self.switchTop = true
                    }

                    if (_self.countTop < _self.constant.ghostLimitStraightLine && _self.switchTop) {
                        randomMove = _self.biasDirectionY === 'ArrowDown' ? 'ArrowUp' : 'ArrowDown'
                        _self.countTop += 1
                    } else if (_self.countTop === _self.constant.ghostLimitStraightLine && _self.switchTop) {
                        _self.countTop = 0
                        _self.switchTop = false
                    }
                }

                if (randomMove === 'ArrowLeft' || randomMove === 'ArrowRight') {

                    if (_self.countRight < _self.constant.ghostLimitStraightLine && !_self.switchLeft) {
                        randomMove = _self.biasDirectionX
                        _self.countRight += 1
                    } else if (_self.countRight === _self.constant.ghostLimitStraightLine && !_self.switchLeft) {
                        _self.countRight = 0
                        _self.switchLeft = true
                    }

                    if (_self.countRight < _self.constant.ghostLimitStraightLine && _self.switchLeft) {
                        randomMove = _self.biasDirectionX === 'ArrowRight' ? 'ArrowLeft' : 'ArrowRight'
                        _self.countRight += 1
                    } else if (_self.countRight === _self.constant.ghostLimitStraightLine && _self.switchLeft) {
                        _self.countRight = 0
                        _self.switchLeft = false
                    }
                }
                _self._super.handleMove(randomMove, _self.constant.stepGhost)
                _self.updatePosition()
            }

            getRandomMove()
        }, 70)
    }

    // Getter.
    getGhost = () => this.ghost

    // Update position.
    updatePosition = () => {

        // Check if the ghost hit pacman vertically.
        if (Math.abs(this.posY - this.pac.getPosY() - this.constant.sizePacman) < 15 ||
            Math.abs(this.pac.getPosY() - this.posY - this.constant.sizePacman) < 15
        ) {
            // Check if pacman is align with ghost laterally.
            if (Math.abs(this.posX - this.pac.getPosX()) < 25) {
                clearInterval(this.intervalId)
                dialogueGameOver(this.floor)
            }
        }

        // Check if the ghost hit pacman laterally.
        if (Math.abs(this.pac.getPosX() + this.constant.sizePacman - this.posX) < 10 ||
            Math.abs(this.posX + this.constant.widthGhost - this.pac.getPosX()) < 10
        ) {
            // Check if pacman is align with ghost vertically.
            if (Math.abs(this.pac.getPosY() - this.posY) < 25) {
                clearInterval(this.intervalId)
                dialogueGameOver(this.floor)
            }
        }
        this.ghost.style.left = `${this.posX - 25}px`
        this.ghost.style.bottom = `${this.posY}px`
    }
}