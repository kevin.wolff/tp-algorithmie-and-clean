import {constant} from './util/constant.js'
import Pacman from './character/Pacman.js'
import dialogueLauncher from './dialogue/dialogueLauncher.js'
import generateScreen from './environment/generateScreen.js'
import generateFloor from './environment/generateFloor.js'
import generateWall from './environment/generateWall.js'
import generateFood from './environment/generateFood.js'
import generateGhost from './environment/generateGhost.js'
import './../sass/index.scss'

const root = document.querySelector('#root')

let launcher = dialogueLauncher()
root.append(launcher)

launcher.childNodes.forEach(difficultyButton => {

    // Listen each difficulty button.
    difficultyButton.addEventListener('click', () => {

        // When player choose a difficulty.
        const gameDifficulty = difficultyButton.innerHTML
        launcher.remove()
        initializeGame(gameDifficulty)
    })
})

// Initialize a game.
function initializeGame(difficulty) {
    const screen = generateScreen()
    const floor = generateFloor()
    const walls = generateWall(constant.wallsInfos)
    const foods = generateFood(constant.foodsPosition, constant.foodWidth, constant.foodHeight, constant.foodColor)
    const pacman = new Pacman(350, 50, constant, floor)

    root.append(screen)
    screen.appendChild(floor)
    walls.forEach(wall => floor.appendChild(wall))
    foods.forEach(food => floor.appendChild(food))
    floor.appendChild(pacman.getPacMan())
    generateGhost(constant, floor, pacman, difficulty)
}