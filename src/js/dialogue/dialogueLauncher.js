// Launcher dialogue.
export default function dialogueLauncher() {

    const difficulties = ['easy', 'medium', 'hard', 'impossible']

    const launcher = document.createElement('div')
    launcher.setAttribute('class', 'modal')

    difficulties.forEach((difficulty, i) => {
        const button = document.createElement('div')
        button.setAttribute('class', 'btn')
        button.innerHTML = `${difficulty}`

        launcher.appendChild(button)
    })

    return launcher
}