// Win dialogue.
export default function dialogueWin(floor) {

    const card = document.createElement('div')
    card.setAttribute('class', 'modal')

    const text = document.createElement('div')
    text.setAttribute('class', 'txt')
    text.innerHTML = "You won!!"

    card.appendChild(text)
    floor.appendChild(card)
}