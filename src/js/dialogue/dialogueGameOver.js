// Game over dialogue.
export default function dialogueGameOver(floor) {

    const card = document.createElement('div')
    card.setAttribute('class', 'modal')

    const text = document.createElement('div')
    text.setAttribute('class', 'txt')
    text.innerHTML = 'Game Over'

    card.appendChild(text)
    floor.appendChild(card)
}