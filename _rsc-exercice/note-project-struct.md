# Notes



## Observation

There are two ways to run JavaScript in a browser :

- include a script for each functionality. This solution is hard to scale because loading too many scripts can cause a network bottleneck. 

- use a big `.js` file containing all your project code, but this leads to problems with scope, size, readability and maintainability.

The project uses this second option, everything (style, script, etc) is contained in one single JavaScript file. In order to make it easily readable and maintainable we consider that split it in multiples chunks seems to be a good option. This option is conceivable thanks to Webpack as it enables us not to face current problems of this solution/option, like network bottleneck.

## Webpack bundler

Webpack works on Node.js, a JavaScript runtime that can be used in computers and servers outside a browser environment. Now that JavaScript is not running in a browser, how are Node  applications supposed to load new chunks of code? There are no HTML files and script tags that can be added to it. CommonJS came out and introduced `require`, which allows us to load and use a module in the current file. This solved scope issues out of the box by importing each module as it was needed.

Webpack is a tool that lets us bundle our JavaScript and it can be extended to support many different asset such as images, fonts and style sheets.

## Solution

For this overhaul we'll introduce Node.js and Webpack who allow us to improve the readability and maintainability without disadvantages cited in observation part. 

### Project architecture

├ *node_modules*

├ *public*

|	├ index.html

|	└ index.js

├ *src*

|	├ *js*

|	|	├ index.js

|	|	├ *class*

|	|	|	├ Ghost.js

|	|	|	├ Pacman.js

|	|	|	└ Character.js

|	|	├ *function*

|	|	|	├ generateWall.js

|	|	|	├ generateGhost.js

|	|	|	└ generateFood.js

|	|	└ *util*

|	|		└ constant.js

|	└ *sass*

|		└ index.scss

├ .gitignore

├ index.html

├ package.json

├ package-lock.json

├ README.md

└ webpack.config.js

## Ressources

[Documentation Webpack](https://webpack.js.org/concepts/modules/#what-is-a-webpack-module)

[Documentation CommonJs](https://en.wikipedia.org/wiki/CommonJS#History)

[Documentation JavaScript module I](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)

[Documentation JavaScript module II](https://www.pierre-giraud.com/javascript-apprendre-coder-cours/module-import-export/)